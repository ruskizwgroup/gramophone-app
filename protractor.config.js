'use strict';

exports.config = {

    seleniumAddress: 'http://localhost:4444/wd/hub',

    allScriptsTimeout: 11000,

    specs: ['./app/tests/e2e/components/sections/grmPlaylist.js'],

    directConnect: true,

    baseUrl: `file://${__dirname}/app/tests/e2e/`,

    onPrepare: function() {
        browser.resetUrl = `file://${__dirname}/app/tests/e2e/reset.html`
    },

    capabilities: {
        'browserName': 'chrome',
        'chromeOptions': {
            'binary':  './node_modules/electron-prebuilt/dist/electron'
        }
    },

    framework: 'jasmine2',

    jasmineNodeOpts: {
        defaultTimeoutInterval: 30000
    },

    plugins: [{
        package: 'protractor-console',
        logLevels: ['severe', 'warning']
    }]
};