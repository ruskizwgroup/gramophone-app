module.exports = {
    context: `${__dirname}/app`,
    entry: [
        './renderer-process/app.js',
        './assets/sass/style.scss',
        './assets/css/style.css',
        'bootstrap-loader'
    ],
    output: {
        path: `${__dirname}/app/dist`,
        filename: 'bundle.js',
        publicPath: '/'
    },
    resolve: {
        modulesDirectories: [
            'node_modules'
        ]
    },
    module: {
        preLoaders: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'jshint-loader'
        }],
        loaders: [{
            test: /\.html$/,
            loader: 'ngtemplate!html'
        },{
            test: /\.css$/, 
            loader: 'style-loader!css-loader' 
        }, { 
            test: /\.scss$/, 
            loaders: ['style', 'css', 'sass']
        }, { 
            test: /.(png|woff(2)?|eot|ttf|svg)(\?[a-z0-9=\.]+)?$/, 
            loader: 'url-loader?limit=100000&name=./fonts/[name].[ext]' 
        }]
    },
    jshint: {
        camelcase: true,
        emitErrors: false,
        failOnHint: false
    },
    externals: [
        (function () {
            'use strict';

            var IGNORES = [
                'electron'
            ];
            return function (context, request, callback) {
                if (IGNORES.indexOf(request) >= 0) {
                    return callback(null, 'require("' + request + '")');
                }
                return callback();
            };
        })()
    ]
};