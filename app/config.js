module.exports = {
    window: {
        options: {
            width: 1080,
            height: 840,
            minWidth: 680,
            icon: `${__dirname}/assets/icons/soundcloud.png`
        },
        startUrl: `file://${__dirname}/index.html`,
        mainProcess: `${__dirname}/main-process/**/*.js`
    }
};