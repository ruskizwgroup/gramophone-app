(function(window){
    'use strict';

    window.jQuery = window.$ = require('jquery');
    
    const angular = require('angular');
    require('@angular/router/angular1/angular_1_router');

    const appTmpl = require('../views/app.html');

    const ngModule = angular.module('app', [
        'ngComponentRouter'
    ])
        .value('$routerRootComponent', 'app')
        .component('app', {
            templateUrl: appTmpl,
            $routeConfig: [
                {path: '/top', name: 'Top', component: 'grmTop', useAsDefault: true},
                {path: '/stream', name: 'Stream', component: 'grmStream'},
                {path: '/likes', name: 'Likes', component: 'grmLikes'},
                {path: '/playlists', name: 'Playlists', component: 'grmPlaylists'}
            ]
        });

    //init components
    require('./components')(ngModule);

})(window);
    