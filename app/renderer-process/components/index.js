'use strict';

module.exports = function (ngModule) {
    require('./sections/grmTop')(ngModule);
    require('./sections/grmLikes')(ngModule);
    require('./sections/grmStream')(ngModule);
    require('./sections/grmPlaylists')(ngModule);
};