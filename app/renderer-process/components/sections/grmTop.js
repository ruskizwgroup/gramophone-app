'use strict';

module.exports = function (ngModule) {

    const topTmpl = require('../../../views/sections/top.html');

    ngModule
        .service('grmTopService', TopService)
        .component('grmTop', {
            templateUrl: topTmpl,
            controller: TopController
        });

    function TopService() {
        //do something
    }

    function TopController() {
        //do something
    }
};