'use strict';

module.exports = function (ngModule) {

    const streamTmpl = require('../../../views/sections/stream.html');

    ngModule
        .service('grmStreamService', StreamService)
        .component('grmStream', {
            templateUrl: streamTmpl,
            controller: StreamController
        });

    function StreamService() {
        //do something
    }

    function StreamController() {
        //do something
    }
};