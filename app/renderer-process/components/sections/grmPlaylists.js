'use strict';

module.exports = function (ngModule) {

    const playlistsTmpl = require('../../../views/sections/playlists.html');
    
    ngModule
        .service('grmPlaylistsService', PlaylistsService)
        .component('grmPlaylists', {
            templateUrl: playlistsTmpl,
            controller: PlaylistsController
        });

    function PlaylistsService() {
        return {
            getText: function (index) {
                if (index === 0) {
                    return 'You selected Yes';
                }

                return 'You selected No';
            }
        };
    }

    PlaylistsController.$inject = ['grmPlaylistsService', '$rootScope'];
    function PlaylistsController(grmPlaylistsService, $rootScope) {
        const ctrl = this;
        const ipc = require('electron').ipcRenderer;

        ctrl.text = 'Text';

        ctrl.showDialog = function () {
            ipc.send('open-information-dialog');
        };

        ipc.on('information-dialog-selection', function (event, index) {
            ctrl.text = grmPlaylistsService.getText(index);
            $rootScope.$digest();
        });
    }
};