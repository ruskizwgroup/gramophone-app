'use strict';

module.exports = function (ngModule) {

    const likesTmpl = require('../../../views/sections/likes.html');
    
    ngModule
        .service('grmLikesService', LikesService)
        .component('grmLikes', {
            templateUrl: likesTmpl,
            bindings: { $router: '<' },
            controller: LikesController
        });

    function LikesService() {
        //do something
    }

    function LikesController() {
        //do something
    }
};