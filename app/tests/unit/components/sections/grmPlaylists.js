'use strict';

describe('grmPlaylistsService Test', function () {
    let getText;

    beforeEach(angular.mock.module('app'));

    beforeEach(angular.mock.inject(function (grmPlaylistsService) {
        getText = grmPlaylistsService.getText;
    }));

    it('getText index=0 test', function () {
        expect(getText(0)).toEqual('You selected Yes');
    });

    it('getText index!=0 test', function () {
        expect(getText(1)).toEqual('You selected No');
    });

});