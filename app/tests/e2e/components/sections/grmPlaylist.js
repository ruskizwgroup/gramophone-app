'use strict';

describe('playlist', function() {
    var h1 = element(by.css('.grm-test-header'));

    beforeEach(function() {
        browser.get('indexTest.html?test=protractor#/likes');
    });

    it('check tag h1 on page', function() {
        expect(h1.getText()).toEqual('Likes');
    });
});