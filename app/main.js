'use strict';

const glob = require('glob');
const electron = require('electron');

const BrowserWindow = electron.BrowserWindow;
const app = electron.app;
const winConf = require('./config').window;

const debug = /--debug/.test(process.argv[2]);

var mainWindow = null;

function initialize() {

    function createWindow() {

        mainWindow = new BrowserWindow(winConf.options);
        mainWindow.loadURL(winConf.startUrl);

        //Debug mode
        if (debug) {
            mainWindow.webContents.openDevTools();
            mainWindow.maximize();
        }

        mainWindow.on('closed', function () {
            mainWindow = null;
        });
    }

    var shouldQuit = makeSingleInstance();

    if (shouldQuit) {
        return app.quit();
    }

    loadMain();

    app.on('ready', function () {
        createWindow();
    });

    app.on('window-all-closed', function () {
        if (process.platform !== 'darwin') {
            app.quit();
        }
    });

    app.on('activate', function () {
        if (mainWindow === null) {
            createWindow();
        }
    });
}

// Make this app a single instance app.
//
// The main window will be restored and focused instead of a second window
// opened when a person attempts to launch a second instance.
//
// Returns true if the current version of the app should quit instead of
// launching.
function makeSingleInstance() {
    return app.makeSingleInstance(function () {
        if (mainWindow) {
            if (mainWindow.isMinimized()) {
                mainWindow.restore();
            }
            mainWindow.focus();
        }
    });
}

// Require each JS file in the main-process dir
function loadMain() {
    var files = glob.sync(winConf.mainProcess);

    files.forEach(function (file) {
        require(file);
    });
}

// Process arguments
switch (process.argv[1]) {
    default:
        initialize();
}
